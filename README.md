## Development Environment
To set up your development environment, install the following software on your local host:
- [the latest Node](https://nodejs.org/en/download/package-manager/)
- [the latest Yarn](https://yarnpkg.com/lang/en/docs/install/)
- [a good Javascript editor](https://www.slant.co/topics/1686/~javascript-ides-or-editors)
- [an editor plugin for Babel](https://babeljs.io/docs/editors)
- [an editor plugin for Standard](https://standardjs.com/#are-there-text-editor-plugins)
- [an editor plugin for Prettier](https://github.com/prettier/prettier#editor-integration)

## Development Scripts
To run a common development script, execute one of the following commands:
- `sudo yarn global add graphcool`: Installs graphcool framework globally.
- `yarn install`: Installs dependencies and dev dependencies.
- `gcf deploy`: Deploys the service to remote graphcool dev environment.
